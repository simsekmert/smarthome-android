package akilliyazilim.ardiunoremoteblink;

import android.os.AsyncTask;

/**
 * Created by mertsimsek on 10/11/14.
 */
public class LightTask extends AsyncTask<Void,Void,Void> {

    String state;

    public LightTask(String state) {
        this.state = state;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        if(state.equals("0"))
            Api.getInstance().turnOnLight();
        else
            Api.getInstance().turnOffLight();
        return null;
    }
}
