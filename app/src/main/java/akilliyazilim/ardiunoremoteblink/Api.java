package akilliyazilim.ardiunoremoteblink;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mertsimsek on 10/11/14.
 */
public class Api {

    public static Api instance = null;

    private Api() {
    }

    public static Api getInstance()
    {
        if(instance == null)
            instance = new Api();
        return instance;
    }

    public void turnOnLight()
    {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://www.akilliyazilim.org/ardiunoledblink/led.php?state=1");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            httpclient.execute(httppost);
        } catch (ClientProtocolException e) {
        } catch (IOException e) {
        }
    }

    public void turnOffLight()
    {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://www.akilliyazilim.org/ardiunoledblink/led.php?state=0");
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            httpclient.execute(httppost);
        } catch (ClientProtocolException e) {
        } catch (IOException e) {
        }
    }


}
