package akilliyazilim.ardiunoremoteblink;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class MyActivity extends ActionBarActivity implements View.OnClickListener {

    Button button;
    String lightstate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(this);
        button.setText("OFF");
        lightstate = "0";

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.button:
                Log.v("TEST",lightstate);
                button.setClickable(false);
                button.setText("Wait...");
                LightTask task = new LightTask(lightstate)
                {
                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        button.setClickable(true);
                        if(lightstate.equals("1"))
                        {
                            button.setText("OFF");
                            lightstate="0";
                        }
                        else
                        {
                            button.setText("ON");
                            lightstate="1";
                        }
                    }
                };
                task.execute();
                break;
        }
    }

}
